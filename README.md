# CoolPool #

The CoolPool is the iOS app which uses Augmented Reality to help the user observe the swimming pool design within the boundaries of their garden.

### What is this repository for? ###

The source code of the iOS app is stored in here

### How do I get set up? ###

Clone the repo and compile it with the XCode.
For Augmented reality to work you need to have compatible Apple device

### Who do I talk to? ###

Get in touch with Rado Danko for more information